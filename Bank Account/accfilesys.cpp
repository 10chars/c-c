#include <iostream>
#include <string>
#include "account.h"
#include "accountfile.h"

using namespace std;

main() {

  AccountFile af;
  int resultok;
  int seekok;
  string command;
  char username[32];
  int idn, money, ordinal;
  
  cout << "Welcome to the account file system!\n";

  resultok = af.afopen("afs.data");
  if (resultok) 
    cout << "Account file opened ok.\n";
  else {
    cout << "Unable to open Account File!\n";
    exit(1);
  }

  while (1) {
    cout << "Command: [list, create, credit, debit, find, print, replace, quit]\n";
    cin >> command;
    if (command == "quit") break;

    else if (command == "list") {
      af.list();
    }

    else if (command == "create") {
      cout << "Account ID number?\n"; cin >> idn; cin.ignore();
      cout << "Account user name?\n"; 
      cin.getline(username, 32);
      cout << "Account initial balance?\n"; cin >> money;
      Account a(idn, username, money);
      resultok = af.seekend();
      if(resultok){
	cout << "The new account will be appended at the end of the file.\n";
	resultok = af.put(a);
	if (resultok)
	  cout << "Account created ok.\n";
	else
	  cout << "Unable to create account.\n";
      }
      else cout << "Unable to seek to the end of the file.\n";
    }

    else if (command == "replace") {
      cout << "Account ID number?\n"; cin >> idn; cin.ignore();
      cout << "Account user name?\n"; 
      cin.getline(username, 32);
      cout << "Account initial balance?\n"; cin >> money;
      Account a(idn, username, money);
    
      cout << "The new account will replace the next one.\n";
      resultok = af.put(a);
      if (resultok)
	cout << "Account created ok.\n";
    
      else
	cout << "Unable to create account.\n";
	
    }

    else if (command == "credit") {
      cout << "Account ID number?\n"; cin >> idn;
      Account a;
      resultok = af.get(idn, a);
      if (resultok) {
	cout << "Account got ok.\n";
	cout << "Amount to credit?\n"; cin >> money;
	a.credit(money);
	cout << "Credit done.\n";

 
         if (af.seekback(a)){
	   resultok=af.put(a);
	 }

	
        if (resultok) cout << "Account put back ok.\n";
	else cout << "Unable to put back account.\n";
      }
      else cout << "Unable to get account.\n";
    }

    else if (command == "debit") {
      cout << "Account ID number?\n"; cin >> idn;
      Account a;
      resultok = af.get(idn, a);
      if (resultok) {
	cout << "Account got ok.\n";
	cout << "Amount to debit?\n"; cin >> money;
	a.debit(money);
	cout << "Debit done.\n";

  
         if (af.seekback(a)){
           resultok=af.put(a);
         }

	if (resultok) cout << "Account put back ok.\n";
	else cout << "Unable to put back account.\n";
      }
      else cout << "Unable to get account.\n";
    }
	
    else if (command == "find") {
      Account a;
      cout << "Record ordinal number? (must be positive integer)\n"; cin >> ordinal;
      if (ordinal > 0){
	resultok=af.find(ordinal, a);
	if (resultok){
	  cout<<"Record got ok.\n";
	  a.print();
	}
	else cout<<"Unable to get record number: " << ordinal << "\n\n";
      }
	  
      else cout << "Record ordinal number has to be greater than zero.\n";
    }


    else if (command == "print") {
      Account a;
      cout << "Account ID number?\n"; cin >> idn;
      resultok = af.get(idn, a);
      if (resultok) {
	cout << "Account got ok.\n";
	cout << "Account printout:\n";
	a.print();
	cout << "Print done.\n";
      }
      else cout << "Unable to get account.\n";
    }
    else
      cout << "Unrecognised command!\n";
  }

  af.afclose();
  cout << "Account file system done.\n";
}
