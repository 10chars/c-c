#include <iostream>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "account.h"
#include "accountfile.h"
#include <stdio.h>

using namespace std;

AccountFile::AccountFile() {
}

int AccountFile::afopen(char *filename) {
  int openretval;

  openretval = open(filename, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
  fd = openretval;
  return (openretval >= 0);
}

int AccountFile::afclose() {
  int closeretval;

  closeretval = close(fd);
  return (closeretval >= 0);
}

void AccountFile::list() {
  Account a;
  int id; 
  int num;
  ssize_t readretval;
  off_t seekval;

  seekval = lseek(fd, 0, SEEK_SET);

  if (seekval < 0) 
    perror("Panic: seek fail in list!");

  num = 0;
  while (1) {
    readretval = read(fd, &a, sizeof(a));

    if (readretval != sizeof(a))
      break;

    id = a.getid();

    cout << "Account ID: " << id << "\n";

    num++;
  }

  cout << "Number of accounts: " << num << "\n\n";
}

int AccountFile::seekend() {
  
  off_t seekretval;

  seekretval = lseek(fd, 0, SEEK_END);
  if (seekretval < 0){
    cout<<"Seek to the endo of file file failed\n\n";
    return 0;
  }
  else return 1;
}


int AccountFile::put(Account &x) {

  int writeval;
  writeval=write(fd, &x, sizeof(x ));
  if (writeval>0) {
    return 1
  }
  return 0;
}

int AccountFile::seekback(Account &x)
{
  off_t offset = sizeof(x);
  off_t backseek = lseek(fd, (0 - offset), SEEK_CUR);
  if (backseek < 0) 
    return 0;
  return 1;
}

int AccountFile::find(int i, Account &x){
  Account y;
  int offset = 0;
  ssize_t readretval;
  off_t seekretval;

  offset=((i-1)*sizeof( x));
  
  seekretval=lseek(fd, offset, SEEK_SET);
  if(seekretval < 0) 
    cout << "There is no such record: " << offset << "\n\n";
  
  readretval = read(fd, &y, sizeof(y));
  if(readretval!=sizeof(y)) 
    return 0;

  x=y;
  return 1;	
}



int AccountFile::get(int i, Account &x) {
  Account y;
  int putid;
  ssize_t readretval;
  off_t seekretval;

  putid = i;

  seekretval = lseek(fd, 0, SEEK_SET);
  if (seekretval < 0) cout << "get initial seek bad!\n";

  while (1) {
    readretval = read(fd, &y, sizeof(y));
    if (readretval != sizeof(y))
      return 0;
    if (putid == y.getid()) {
      x = y;
      return 1;
    }
  }
}
