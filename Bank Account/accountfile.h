#ifndef ACCOUNTFILE_H
#define ACCOUNTFILE_H

class AccountFile {
 public: 
  AccountFile();
  int afopen(char *filename);
  int afclose();
  void list();
  int put(Account &x);
  int seekback(Account &x);
  int get(int i, Account &x);
    int find(int i, Account &x);
    int seekend();
 private:
    int fd;
};

#endif

